# Install methods

If you're here its probably because you'd like to begin working with and using Linux <3.

But there are a few different ways to install Linux depending on what you want to end up with.

Here we'll outline the different ways you can install Linux, and which might suit you best.

## Pure Linux

This is where you fully replace your current OS with Linux. This means bye-bye to Windows and only Linux.

## Dualboot

Probably the most popular option, this is where you get to have multiple operating systems on your device at once. You'll do this by *partitioning* your hard drive into a Windows section and a Linux section, then each time you turn on your device you get to choose which to use. This means you can have the best of both worlds.

## Virtual Machine (VM)

A Virtual Machine is a simulation of a computer, on a computer. It's a great way of playing around with an OS you want to try out but you're not sure about, or for performing experiments on.

Operating systems definitely run slower and on virtual machines than on the bare metal, but if you don't want to switch your main operating system, but still be able to have a linux machine to do experiments / projects on, this might be a good option.

## Windows Subsystem for Linux (WSL)

This is an interesting one. Windows recently(ish) added a "Linux Subsystem" to Windows effectively adding the ability for you to run Linuxy stuff on a Windows kernel. It's without the speed drawbacks of a VM but also without the advantages of having a full linux OS/kernel/filesystem.


# Distributions

Linux comes in different flavours or "Distributions" (aka distros). While it's the same *kernel* (core of an operating system) they can each work differently on top e.g how software gets installed, how networking works, how it looks. There are endless distros out there if you want, but to make things simpler we've chosen to write this guide for **2**. These are:

## Ubuntu

![Ubuntu](images/ubuntu.jpeg)

Maybe the most popular distribution out there, Ubuntu is a really great distro. It looks great and is easy to use, it's kept up to date and has great support for packages and software.
Just because it's "good for beginners" doesn't mean it's any less advanced / capable than any other distro, remember they're all built on the same underlying kernel, it's just *how* things get done on top of that that changes.

## Manjaro

![Manjaro](images/manjaro.png)

Manjaro is an open source distro built around Arch Linux: a distribution that focuses on a DIY attitude meaning you get to fine tune every bit of how it works. However Manjaro is basically "Arch that works out of the box", so is completely beginner friendly. Read more about it here: [manjaro.org](https://manjaro.org/)

