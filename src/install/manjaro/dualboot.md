# Manjaro Dualboot

<span style="color:red; font-weight:bold">Warning: Please make sure
you have made a backup of all the data you do not want to loose.</span>

If you haven't checked out the [Dualboot preinstall
checks](../../dualboot-preinstall.md), **go back and go through them**.

**Please make sure you have a backup of any data you do not want to lose. We
are providing the instructions as they are, and we are not responsible for
any data loss that might occur. Use at your own risk.**

This guide will walk you through how to install Manjaro as dual boot, alongside
your existing Windows OS.

It assumes you have already shrunk your Windows partition and correctly set 
the boot order. If you haven't then you still didn't read the pre-install
checks T_T **Go read them!**

## Step 1 (Preparation)

Before starting, [download Manjaro](https://manjaro.org/downloads/official/gnome/).

You'll get an ISO file, which is a disk image. You need to flash this to a
USB drive, using an imager tool. The Raspberry Pi foundation keeps an
up-to-date list of [imagers for Windows](https://www.raspberrypi.org/documentation/installation/installing-images/windows.md),
however, we recommend [Etcher](https://www.balena.io/etcher/). [Rufus](https://rufus.ie/) is another good ISO burner.

In Etcher:
- Select your ISO file
- Select your USB drive (plug it in!)
- Then flash!

![Etcher](../../images/manjaro/etcher.png)

## Step 2 (Boot)

To boot into the live installation media, plug your USB into your
computer/laptop, and reboot - you should be able to boot off it with no
problems.

Once boot has finished, you should be presented with the Manjaro Hello utility!

![Installer](../../images/manjaro/manjaro_hello.png)

Close it and play around with the system. Make sure everything works (keyboard, trackpad).
You can also connect to the wifi:

![Wifi](../../images/manjaro/wifi.png)

And choose the network:

![Network](../../images/manjaro/choose_network.png)

Once you're happy everything works okay, you can proceed to install it. Click the
disk w/ a green arrow on the left hand side bar. This will start the installer.
Choose the language:

![Language](../../images/manjaro/installer_language.png)

Your region:

![Installer Region](../../images/manjaro/installer_region.png)

And your keyboard layout:

![Installer Keyboard](../../images/manjaro/installer_keyboard.png)

Partitioning time! Choose **Replace a partition** and click on the gray area 
(the free space you created when shrinking the Windows partition). Click Next.

![Partitions](../../images/manjaro/manjaro_dualboot_partitions.png)

The final screen will confirm all your options. Now click Install and wait.

![Confirm install](../../images/manjaro/manjaro_dualboot_confirm.png)

Once it's completed, follow the prompts to shutdown, remove the installation
media, and restart your computer. When you startup, you should be booted into
Manjaro!

![Neofetch](../../images/manjaro/dualboot-neofetch.png)

And remember, the most important think about using Arch is to tell people as 
often as possible 

`btw, I use Arch` &#128516;

Now head over to the [Post Installation](../../post-install.md) guide to update your
system and install some useful software.
