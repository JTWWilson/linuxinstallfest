# Post installation

Things you might want to do after installing linux, common software choices,
etc.

## Adjusting your default boot order

Now you've installed Linux remember to go back and readjust your boot order.

If you've dualbooted you'll now have 2 possible partitions to boot into at startup: Windows or Linux, as well as USB etc.

Importantly: if the BIOS boots into windows it'll startup Windows straight away, but if you set it to boot to linux it will actually go to GRUB from which you can select your OS. 

So restart your device, press the key to enter BIOS and go back to the "Boot Order" menu. Move Linux to the top and Windows to 2nd place. You can safely remove the installation media now. The order should now look something like this:

![BIOS boot order](images/bios-bootorder.jpg)

Then when it boots from now you will end up in a menu such as:

![GRUB boot selection](images/grub.jpg)

See how you can now choose between linux and windows every time you boot.

## Updating

You should keep your system up-to-date, and try and run updates every so
often. Updates aren't as naggy as in Windows, so you may find yourself
needing to remember to install them.

**Notice:** Make sure to be connected to an active internet connection, and
*prepare for the command to take a while to finish executing.

In Ubuntu (with apt):

    sudo apt update && sudo apt upgrade

**Note for Manjaro**: It is good practice to keep your system up to date
as often as possible. Manjaro does not have *version releases* like Ubuntu. 
It is a **rolling updates** distro. It is also good practice to check the
[Arch Linux News](https://www.archlinux.org/news/) before an update. If 
a major update (e.g. kernel update) breaks something, it will be posted
there, and you'll know to wait a few days such that it's solved.

Also, the [Arch Wiki](https://wiki.archlinux.org/) is your friend for
any Manjaro related questions. As we said, Manjaro is based on Arch, so
most things there apply to it as well!

In Manjaro (with pacman):

    sudo pacman -Syu

Lastly, for Manjaro, one of the great things about it is the 
[Arch User Repository](https://aur.archlinux.org/). This contains
packages *contributed by the community*. As the AUR disclaimer says,

> AUR packages are user produced content. Any use of the provided 
> files is at your own risk.

So please, before you install a package from AUR, read its page and 
check the Comments section for known issues. As the packages are user
contributed, remember, they can be questionable. Make your own decisions.

In order to install packages from AUR, you should use a helper program
that will manage the installation and upgrades for you. `yay` is a great
one. You can install it with:

    pacman -S --needed git base-devel
    git clone https://aur.archlinux.org/yay.git
    cd yay
    makepkg -si

Now you can start intalling AUR packages with yay! It behaves similarly to
pacman.

## Installing software

In Ubuntu (with apt):

    sudo apt install <package>

**Note:** you may see both `apt` and `apt-get` around. `apt` is the *new* form
*of `apt-get` and should be preferred in general (though not for scripts).

In Manjaro (with pacman):

    sudo pacman -S <package>

(with yay):

    yay -S <package>

## Firewalling

Linux doesn't come with any preinstalled firewalls, this means devices on a shared network might be able to access any webservers you accidentally leave running, as well as SSH. SSH is probably the one listening service that will run by default on a Linux box and if you have poor passwords this is a risk.

So it's a good idea to install a super simple firewall that just stops anything connecting to your machine, don't worry it won't stop outgoing connections, just incoming attempts.

Follow the short [UFW guide](./post-install/ufw.md) to get this set up.

## Change your default shell

Lots of us tend to prefer zsh over bash. It's faster, more customizable, and
usually has fewer pain points.

To install it:

    sudo apt install zsh

or

    sudo pacman -S zsh

Then:

    chsh -s /bin/zsh

## Theming

One of the best things about using Linux is the awesome levels of
customisability!

How you customize the look and feel depends on what desktop environment
you've chosen, for Ubuntu, it's most likely GNOME, for Manjaro, it's most
likely KDE, but check to make sure.

Check out the settings menus in both!

### GNOME

For GNOME, you can install GNOME Tweak Tool to be able to change themes:

    sudo apt install gnome-tweak-tool

or

    sudo pacman -S gnome-tweaks

## Customise GRUB (Advanced)

You can even customise the bootloader you see for choosing the OS.

[Gnome-look.org](https://www.gnome-look.org/browse/cat/109/order/latest/) has
a lot of nice themes. Don't ask us how to do this, you'll have to learn it on
your own! We're just giving you ideas. Google is your friend :-)

[Fallout theme GRUB](https://www.gnome-look.org/p/1230882/)
![Fallout theme](./images/grub_fallout.png)

[Plasma Dark theme GRUB](https://www.gnome-look.org/p/1195799/)
![Fallout theme](./images/grub_plasma.png)

## Useful packages

+ [Chromium Web Browser](./post-install/chromium.md) - Chromium is an open-source web browser.
+ [Java Development Kit (JDK)](./post-install/java.md) - Java is a is a general-purpose programming language that is taught in first year Computer Science at UoB. 
+ [Neofetch](./post-install/neofetch.md) - Neofetch is a command that lists information about your Linux distribution and the machine you are running on.
+ [Speedtest](./post-install/speedtest.md) - Speedtest is a command that tests how fast your internet is via the terminal.
 
