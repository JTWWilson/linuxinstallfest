# Troubleshooting 

If you have any problem, let us know on [Discord](https://discord.com/invite/EkcRZc4).

If we notice many people encoutering the same problem, we'll quickly write up
how to fix it and post it here.

For now, we hope the guide is good enough to get you through a successful Linux
installation :-)

